#
# Common synthesis script
#
source -notrace tcl/proc.tcl
#source -notrace tcl/aurora_64b66b_0.tcl
source -notrace tcl/project_cfg.tcl
#
# Read Timing Constraints file
read_xdc -unmanaged tcl/project_timing.xdc
#
# Turn off some of the non-helpful messages
source -notrace tcl/suppress.tcl
#
# Run synthesis
#
# NOTE: Currently set to RuntimeOptimized and no_timing_driven to get the shortest run time.
#        and opt_design is turned off. (change as needed)
#
puts ""
puts "-----------------------------------------------------------"
puts "Running synthesis on $top_level"
puts "-----------------------------------------------------------"
#synth_design -top $top_level -part $part_num -directive RuntimeOptimized -no_timing_driven -verilog_define C_TEST_AURORA_ON
synth_design -top $top_level -part $part_num -directive RuntimeOptimized -verilog_define C_TEST_AURORA_ON
#opt_design
write_checkpoint -force dcp/syn
#report_timing_summary -file rpt/syn_timing_summary.rpt

# Post run scripts
# --------------------------------------------------------------------

# Pull out of the log file all errors and critical warnings for easy viewing
search_for_msgs syn

# End of script
