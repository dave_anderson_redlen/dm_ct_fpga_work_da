#
# Clean up script
#
puts "\nRemove Tool generated Files"

file delete -force -- bit/*
file delete -force -- dcp/*
file delete -force -- log/*
file delete -force -- rpt/*
file delete -force -- .Xil
file delete fsm_encoding.os

#
# END
#
