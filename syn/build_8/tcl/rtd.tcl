#
# Common Route Design script
#
#
source tcl/proc.tcl
source tcl/project_cfg.tcl
#
# Read in design
open_checkpoint dcp/pld.dcp
#
# run router, report actual utilization and timing, write checkpoint design,
# run drc, write verilog and xdc out
# NOTE: Currently set to quick to get the shortest run time. (change as needed)
#
puts ""
puts "-----------------------------------------------------------"
puts "Running Design Routing on $top_level"
puts "-----------------------------------------------------------"
route_design -directive Quick
write_checkpoint -force dcp/rtd
report_timing_summary -file rpt/pld_timing_summary.rpt
report_timing -sort_by group -max_paths 100 -path_type summary -file rpt/pld_timing.rpt
report_clock_utilization -file rpt/clock_util.rpt
report_utilization -file rpt/rtd_util.rpt
report_drc -file rpt/rtd_drc.rpt
write_verilog -force net/project_tool_gen_netlist.v
write_xdc -no_fixed_only -force net/project_tool_gen.xdc
