# Turn off some of the non-helpful messages
#
set_msg_config -id "Synth 8-3886" -suppress
set_msg_config -id "Synth 8-3333" -suppress
set_msg_config -id "Synth 8-3332" -suppress
set_msg_config -id "Synth 8-6014" -suppress
# WARNING: [Synth 8-5788] Register _ in module _ is has both Set and reset with same priority.
set_msg_config -id "Synth 8-5788" -suppress
