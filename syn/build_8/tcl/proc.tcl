#
# Common Procedures for all TCL scripts.
#

#
# Procedure to create a new directory. First check if the dir exists.
#
proc create_dir {dir_name} {
	if {[file isdirectory ./$dir_name] < 1} { file mkdir $dir_name}
}

#
# This procedure opens a file a file for reading or writting.
# file_in is the complete name of the file plus path.
# mode = +r, +w, or +a
#
proc open_file {file_in mode} {
	if {[catch {set fileId_in [open $file_in $mode]} err]} {
		puts "open failed: $err"
		return
	} else {
		puts "Opening '$file_in' "
		return $fileId_in
	}
}

#
# This procedure takes in the name of a log file and searchs that file for error and critical warnings.
# The list of messages is written to a second log file called *_err.log where * is input name.
# name is just the first part (i.e syn, pld,..). The proc assumes the path is log/ and assumes the ext is .log
#
proc search_for_msgs {input_log_name} {
	# Open the log file and the output file for the error messages
	set input_log_name syn
	set file_out ""
	append file_out $input_log_name "_err"
	set fileId_in [open_file log/$input_log_name.log "r+"]
	set fileId_out [open_file log/$file_out.log "w+"]
	puts $fileId_out "Error Msg Search Results of $input_log_name.log File\n"
	set error_list {}
	set critical_list {}

	# Read each line in the file and look for errors and critical warnings
	set i 1
  set found_one 0
	while {![eof $fileId_in]} {
		# Read a line from the file, and assign it to the $line variable
		set line [gets $fileId_in]
		set first_word [scan $line %s]
		if {$first_word == "ERROR:"} {
			lappend error_list "Line $i: $line"
			set found_one 1
	  }
		if {$first_word == "CRITICAL"} {
			lappend critical_list "Line $i: $line"
			set found_one 1
	  }
		incr i
	}

	# first show errors
	for {set i 0} {$i < [llength $error_list]} {incr i} {
		puts $fileId_out [lindex $error_list $i]
	}
		# Then show critical warnings
	for {set i 0} {$i < [llength $critical_list]} {incr i} {
		puts $fileId_out [lindex $critical_list $i]
	}
	# Check if we found any errors
	if {$found_one != 1} {
		puts $fileId_out "!!! No Errors found !!! \n"
	}
	close $fileId_in
	close $fileId_out

	return 1
}
