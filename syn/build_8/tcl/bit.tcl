#
# Common bitstream generation script
#
#
source tcl/proc.tcl
source tcl/project_cfg.tcl
#
# Read in implemented design
open_checkpoint dcp/rtd.dcp
#
# Don't show message about no loads
set_msg_config -id "DRC RTSTAT-10" -suppress
#
#
# Just generate the .bit file. Nothing else needs to be generated at this point.
#
puts ""
puts "-----------------------------------------------------------"
puts "Generating Bit file for Design $top_level"
puts "-----------------------------------------------------------"
write_bitstream -force bit/project.bit
