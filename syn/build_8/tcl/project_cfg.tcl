#
# Project file - Reads in all code files for project
#
# Set the name of the top_level entity here:
set top_level dm_top_level
#
# Set the part number here:
#set part_num xcku025-ffva1156-2-i
set part_num xc7k160tfbg484-2
#
# Check if output directories exist, if not then create the structure.
#
create_dir bit
create_dir log
create_dir dcp
create_dir rpt
create_dir net
#
# ###################################################################
# Get all project Files
#
set core_path ../../../core
set dev_path ../rtl
#
# Read in top level file
#read_verilog $dev_path/dm_top_level.v
#
# This is just the I/O's. I used it to pipe-clean the pinout xdc file.
# read_verilog $dev_path/dm_top_level_dbq_mod.v
#
# COMMON Files
read_verilog $core_path/COMMON/rtl/ram_2kx18.v
read_verilog $core_path/COMMON/rtl/crc_calc_16.v
read_verilog $core_path/COMMON/rtl/ram_generic.v
read_verilog $core_path/COMMON/rtl/lfsr_counter_140k.v
#
# Aurora
read_verilog $core_path/AURORA/rtl/aurora.v
read_verilog $core_path/AURORA/rtl/aurora_bb.v
read_verilog $core_path/AURORA/rtl/aurora_reset.v
read_verilog $core_path/AURORA/rtl/lfsr_counter_45ms.v
#
# Framer
read_verilog $core_path/FRAMER/rtl/framer.v
read_verilog $core_path/FRAMER/rtl/reg_framer.v
read_verilog $core_path/FRAMER/rtl/test_framer.v
read_verilog $core_path/FRAMER/rtl/retry_controller.v
read_verilog $core_path/FRAMER/rtl/rx_controller.v
read_verilog $core_path/FRAMER/rtl/tx_controller.v
read_verilog $core_path/FRAMER/rtl/tx_framer.v
#
# Quad Memory
read_verilog $core_path/QUAD_MEM/rtl/quad_mem.v
read_verilog $core_path/QUAD_MEM/rtl/mem_wr_cntr.v
read_verilog $core_path/QUAD_MEM/rtl/ram_block.v
read_verilog $core_path/QUAD_MEM/rtl/adder.v
#
# ASIC Recever module
read_verilog $core_path/DM_RECV2/rtl/quad_recv.v
read_verilog $core_path/DM_RECV2/rtl/dm_recv.v
read_verilog $core_path/DM_RECV2/rtl/asic_recv.v
read_verilog $core_path/DM_RECV2/rtl/pattern_search.v
read_verilog $core_path/DM_RECV2/rtl/address_gen2.v
#
# Read in sub modules where all the code in dir is used
#
read_verilog [ glob $core_path/ASIC_SPI/rtl/*.v]
read_verilog [ glob $core_path/DM_MON/rtl/*.v]
read_verilog [ glob $core_path/DM_TRIG/rtl/*.v]
read_verilog [ glob $core_path/PULSER/rtl/*.v]
#
# SPI Flash module project files
read_verilog $core_path/DM_FLASH/rtl/asic_config_flash_ctrl.v
read_verilog $core_path/DM_FLASH/rtl/asic_config_ctrl.v
read_verilog $core_path/DM_FLASH/rtl/spi_flash_ctrl.v
read_verilog $core_path/DM_FLASH/rtl/lfsr_counter_300.v
#
#
# Reprogram SPI Flash module
read_verilog $core_path/DM_REPROGRAM/rtl/reprogram_flash_ctrl.v
read_verilog $core_path/DM_REPROGRAM/rtl/reprogram_ctrl.v
read_verilog $core_path/DM_REPROGRAM/rtl/spi_flash_ctrl_adesto.v
read_verilog $core_path/DM_REPROGRAM/rtl/lfsr_counter_50.v
#
# Control Register module
read_verilog $core_path/REG_CONTROL/rtl/dm_spi_slave.v
read_verilog $core_path/REG_CONTROL/rtl/dm_registers.v
read_verilog $core_path/REG_CONTROL/rtl/asic_shadow_ram.v
read_verilog $core_path/REG_CONTROL/rtl/calibration_chain_ram.v
read_verilog $core_path/REG_CONTROL/rtl/threshold_chain_ram.v
read_verilog $core_path/REG_CONTROL/rtl/chain_ram.v
#
# Test pattern generated used in ASIC Recever module
read_verilog $core_path/TEST_PAT_GEN/rtl/test_pat_gen.v
#
#
# END OF FILE
