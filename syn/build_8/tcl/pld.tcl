#
# Common Placement script
#
#
source -notrace tcl/proc.tcl
source -notrace tcl/project_cfg.tcl
#
# Turn off some of the non-helpful messages
#
# Read in design
open_checkpoint dcp/syn.dcp
#
# Read Pin placement Constraints file
read_xdc -unmanaged -verbose tcl/project_pinout.xdc
write_xdc -force -constraints invalid rpt/xdc_invalid.rpt
#
# Run Placement,
# NOTE: Currently set to quick to get the shortest run time. (change as needed)
#
puts ""
puts "-----------------------------------------------------------"
puts "Running Design Placement on $top_level"
puts "-----------------------------------------------------------"
place_design -directive Quick
# phys_opt_design
#write_checkpoint -force dcp/pld
#report_timing_summary -file rpt/pld_timing_summary.rpt
report_io -file rpt/io.rpt
report_clocks -file rpt/clocks.rpt
report_clock_utilization -file rpt/clk_utl.rpt
