
####################################################################################
# Design: dm_top_level_adapter
#
####################################################################################

# Timing paths


# False path settings


# Define clocks

create_clock	-name DM_SYSCLK			-period 28.56	[get_ports	DM_SYSCLK_P]
create_clock	-name ASIC_DCLK0 		-period 7.14 	[get_ports ASIC0_DCLK_P]
create_clock	-name ASIC_DCLK1 		-period 7.14 	[get_ports ASIC1_DCLK_P]
create_clock	-name ASIC_DCLK2 		-period 7.14 	[get_ports ASIC2_DCLK_P]
create_clock	-name ASIC_DCLK3 		-period 7.14 	[get_ports ASIC3_DCLK_P]
create_clock	-name ASIC_DCLK4 		-period 7.14 	[get_ports ASIC4_DCLK_P]
create_clock	-name ASIC_DCLK5 		-period 7.14 	[get_ports ASIC5_DCLK_P]
create_clock	-name ASIC_DCLK6 		-period 7.14 	[get_ports ASIC6_DCLK_P]
create_clock	-name ASIC_DCLK7 		-period 7.14 	[get_ports ASIC7_DCLK_P]
